Тестовое задание для банка точка
==============

Задание состоит из трёх частей

за первые 2 задания отвечают классы Cbr() и CbrDb()
Хотелось бы добавить класс работы с Валютой для строгой типизации ввода/вывода данных, но для этого не так много времени. 

верстка по макету в процессе, /html/index.html

страница-аналог табло курсов валют tablo.php
динамика курсов валют curs.php

new Cbr()->getLastDate()
==
получаем последнюю дату 


new Cbr()->getCursOnDay($date,array($valCode))
==
получаем курсы валют на дату
valCode - символьные обозначения валюты('EUR','USD')

new Cbr()->getValuteCode()
==
получаем список валют
в частности нас интересует код валюты
$val = new Cbr()->getValuteCode();
$code = $val['USD']['Vcode'];

new Cbr()->getCursDynamic(date_from,date_to,code);
==
Динамика изменения курса за период времени
далее с загруженными данными мы можем выполнить 

new CbrDb()->updateValutes($data);
==
update динамики валюты за дату

new CbrDb()->saveValutes($data);
==
insert динамики валюты на дату
для более быстрой работы, холодного старта


new CbrDb()->getValutes(date_from,date_to,code)
==
получаем данные из базы


для создания базы данных new CbrDb()->createDB()

для создания базы таблицы new CbrDb()->сreateDBTable()
