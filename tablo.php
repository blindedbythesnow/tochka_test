<?php
/*
 * Текущий курс валют
 */
include_once 'vendor/autoload.php';

$curDate = (new Cbr())->getLastDate();


$curs = (new Cbr())->getCursOnDay($curDate,array('USD','EUR'));
echo '<p>Курс валют на '.date('d.m.Y', strtotime($curDate));
foreach ($curs as $valCode=>$valValue){
    echo "<p><b>$valCode:</b> $valValue</p>";
}

$val = (new Cbr())->getValuteCode();
$code = $val['USD']['Vcode'];

$curs = (new Cbr())->getCursDynamic(date('c',strtotime('10 days ago')),date('c'),$code);

//to fill database
//$db = (new CbrDb())->updateValutes($curs);
$res = (new CbrDb())->getValutes(date('c',strtotime('10 days ago')),date('c'),$code);


echo '<p>Динамика курса USD за период с  '.date('d.m.Y',strtotime('10 days ago')).' по '.date('d.m.Y');

foreach ($res as $valValue){
    $data = date('d.m.Y',  strtotime($valValue->date));
    echo "<p><b>$data:</b> $valValue->value</p>";
}