<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CurrentDateCbrr
 *
 * @author admind
 */
class Cbr {

    //put your code here

    public function __construct() {
        
    }

    /**

     * 
     * @return date
     */
    
    function getLastDate() {

        $soap = CbrFactory::getFactory()->getSoap();

        if (!$soap) {

            return;
        }
        $lastDate = $soap->GetLatestDateTime()->GetLatestDateTimeResult;


        return $lastDate;
    }

    /**

     * 
     * @param $date date('c')
     * @param $curs array('CODE')
     * @return array('CODE'=>'Value');
     */
    public function getCursOnDay($date, $curs = array('EUR', 'USD')) {

        $cursVal = array();

        $params = array('On_date' => $date);
        //$params = array('On_date' => $this->getLastDate());
        $soap = CbrFactory::getFactory()->getSoap();
        if (!$soap) {
            return;
        }
        $response = $soap->GetCursOnDate($params);

        if (!empty($response->GetCursOnDateResult->any)) {

            $xml = new SimpleXMLElement($response->GetCursOnDateResult->any);

            if (!empty($xml->ValuteData->ValuteCursOnDate) && $xml->ValuteData->ValuteCursOnDate instanceof Traversable) {

                foreach ($xml->ValuteData->ValuteCursOnDate as $Value) {
                    # code...

                    if (in_array($Value->VchCode, $curs)) {


                        $cursVal[(string) $Value->VchCode] = (string) $Value->Vcurs;
                    }
                }
            } else {
                error_log('fail fetch soap ValuteCursOnDate on date ' . $date, 1, Config::get('DEBUG_EMAIL'), 'soap trouble');
            }
        } else {
            error_log('fail fetch soap GetCursOnDate on date ' . $date, 1, Config::get('DEBUG_EMAIL'), 'soap trouble');
        }

        return $cursVal;
    }
    /**

     * 
     * @param $from_date date('c');
     * @param $to_date date('c');
     * @param $code valute Code
     * @return array     /
     */
    
    public function getCursDynamic($from_date,$to_date,$code) {

        
        $date = date('c');

        $params = array(
            'FromDate' => $from_date,
            'ToDate' => $to_date,
            'ValutaCode' => $code,
                );
        
        
        $soap = CbrFactory::getFactory()->getSoap();
        if (!$soap) {
            return;
        }
        
        $response = $soap->GetCursDynamic($params);
        

        if (!empty($response->GetCursDynamicResult->any)) {

            $xml = new SimpleXMLElement($response->GetCursDynamicResult->any);
            
            if (!empty($xml->ValuteData->ValuteCursDynamic) && $xml->ValuteData->ValuteCursDynamic instanceof Traversable) {

                foreach ($xml->ValuteData->ValuteCursDynamic as $Value) {

                        $date = explode('T',$Value->CursDate)[0];
                        $curs = (string) $Value->Vcurs;
                        //$cursVal[$date] = $curs;
                        $cursVal[] = array($params['ValutaCode'],$date,$curs);
                    
                   
                    
                }
            } else {
                echo 'fail fetch soap ValuteGetCursDynamic on date ';
                error_log('fail fetch soap ValuteCursOnDate on date ' . $date, 1, Config::get('DEBUG_EMAIL'), 'soap trouble');
                return;
            }
        } else {
            
            echo 'fail fetch soap GetCursDynamic on dat';
            error_log('fail fetch soap GetCursOnDate on date ' . $date, 1, Config::get('DEBUG_EMAIL'), 'soap trouble');
            return;
        }

        return $cursVal;
    }
    
    public function getValuteCode() {
        
        $Valutes = array();
        
        $soap = CbrFactory::getFactory()->getSoap();
        if (!$soap) {
            return;
        }
        
        $params = array('Seld' => false);
        
        $response = $soap->EnumValutes($params);
        
        if(empty($response->EnumValutesResult->any)){
            echo 'fail fetch soap EnumValutes ';
            return;
        }
        
        $xml = new SimpleXMLElement($response->EnumValutesResult->any);
            
            if (!empty($xml->ValuteData->EnumValutes) && $xml->ValuteData->EnumValutes instanceof Traversable) {

                foreach ($xml->ValuteData->EnumValutes as $Value) {

                        $date = explode('T',$Value->CursDate)[0];
                        $curs = (string) $Value->Vcurs;
                        $cursVal[$date] = $curs;
                        
                        $Vcode = (string) trim($Value->Vcode);
                        $Vname = (string) $Value->Vname;
                        $VEngname = (string) $Value->VEngname;
                        $VcharCode = (string) $Value->VcharCode;
                        $VnumCode = (string) $Value->VnumCode;
                        $Vnom = (string) $Value->Vnom;
                        
                    $Valutes[$VcharCode] = array(
                        'Vcode'=>$Vcode,
                        'Vname'=>$Vname,
                        'VEngname'=>$VEngname,
                        'VcharCode'=>$VcharCode,
                        'VnumCode'=>$VnumCode,
                        'Vnom'=>$Vnom,
                        );
                    
                }
            }else{
                return ;
            }
            
        return $Valutes;
    }
    
    

}
