<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cbrr
 *
 * @author admind
 */
class CbrFactory {

    //put your code here

    const WDSL = 'http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL';

    private static $cbrr;
    private $soap;

    public static function getFactory() {
        if (!self::$cbrr) {
            self::$cbrr = new CbrFactory();
        }
        return self::$cbrr;
    }

    public function getSoap() {
        
        if (!$this->soap) {
            try {
                //$response = $client->GetCursOnDate($params);
                $this->soap = new SoapClient(self::WDSL, array('soap_version' => SOAP_1_2));
            } catch (Exception $e) {
                echo 'Выброшено исключение: ', $e->getMessage(), "\n";
                return ;
            }
        }
        return $this->soap;
    }

}
