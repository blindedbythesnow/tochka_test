<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CbrDb
 *
 * @author admind
 */
class CbrDb {

    //put your code here

    public function createDB() {

        try {
            $pdo = new PDO("mysql:host=" . Config::get('DB_HOST'), Config::get('DB_USER'), Config::get('DB_PASS'));

            $pdo->exec("CREATE DATABASE `" . Config::get('DB_NAME') . "`;")
                    or die(print_r($pdo->errorInfo(), true));
            /*
             * CREATE USER '".Config::get('DB_USER')."'@'localhost' IDENTIFIED BY '".Config::get('DB_PASS')."';
              GRANT ALL ON `".Config::get('DB_NAME')."`.* TO '".Config::get('DB_USER')."'@'localhost';
              FLUSH PRIVILEGES;
             */

            return true;
        } catch (PDOException $e) {
            var_dump("DB ERROR: " . $e->getMessage());
        }

        return false;
    }

    public function createDBTable() {

        $pdo = DatabaseFactory::getFactory()->getConnection();

        try {

            $result = $pdo->query("create table IF NOT EXISTS `valutes_on_date` (
                id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                valuta_code VARCHAR(30) NOT NULL,
                `date` DATE NOT NULL,
                `value` VARCHAR(30) NOT NULL
                )
                ");

            return true;
        } catch (Exception $e) {
            var_dump("DB ERROR: " . $e->getMessage());
        }

        return false;
    }

    public function dropValutes($from_date, $to_date, $val_code) {

        $pdo = DatabaseFactory::getFactory()->getConnection();

        $sql = "select 
             `date`,`valuta_code`,`value` 
             from `valutes_on_date` 
             where `date`>=:date_from and `date`<=:date_to and valuta_code=:valuta_code ";

        $stmt = $pdo->prepare($sql);
        $stmt->execute(array(':date_from' => $from_date, ':date_to' => $to_date, ':valuta_code' => $val_code));

        return $result;
    }

    public function updateValutes($data = array()) {

        $pdo = DatabaseFactory::getFactory()->getConnection();
        $pdo->beginTransaction(); // also helps speed up your inserts.

        foreach ($data as $curs) {

            //var_dump($curs);exit;
            $sql = "select id from `valutes_on_date` where `date`=:date and valuta_code=:valuta_code limit 1";
            $stmt = $pdo->prepare($sql);
            $stmt->execute(array(':date' => $curs[1], ':valuta_code' => $curs[0]));


            if (!$stmt->fetch() > 0) {

                $sql = "insert into `valutes_on_date` (`valuta_code`,`date`,`value`) values (:valuta_code,:date,:value)";
                $stmt = $pdo->prepare($sql);

                try {
                    $stmt->execute(array(':date' => $curs[1], ':valuta_code' => $curs[0], ':value' => $curs[2]));
                } catch (PDOException $e) {
                    echo $e->getMessage();
                    $pdo->rollBack();
                    die();
                }
            }
        }
        $pdo->commit();


        return;
    }

    public function saveValutes($data = array()) {

        $pdo = DatabaseFactory::getFactory()->getConnection();
        $pdo->beginTransaction(); // also helps speed up your inserts.

        $datafields = array('valuta_code', 'date', 'value');
        $insert_values = array();
        /*
          $data = array(
          array('R01235','2016-01-01','12345'),
          array('R01235','2016-01-02','12346'),
          );
         */

        foreach ($data as $d) {
            $question_marks[] = '(' . self::placeholders('?', sizeof($d)) . ')';
            $insert_values = array_merge($insert_values, array_values($d));
        }

        $sql = "INSERT INTO `valutes_on_date` (" . implode(",", $datafields) . ") VALUES " . implode(',', $question_marks);


        $stmt = $pdo->prepare($sql);
        try {
            $stmt->execute($insert_values);
        } catch (PDOException $e) {
            echo $e->getMessage();
            $pdo->rollBack();
        }
        $pdo->commit();


        return;
    }

    public function getValutes($from_date, $to_date, $val_code) {

        $pdo = DatabaseFactory::getFactory()->getConnection();

        $sql = "select 
             `date`,`valuta_code`,`value` 
             from `valutes_on_date` 
             where `date`>=:date_from and `date`<=:date_to and valuta_code=:valuta_code ";

        $stmt = $pdo->prepare($sql);
        $stmt->execute(array(':date_from' => $from_date, ':date_to' => $to_date, ':valuta_code' => $val_code));
        $result = $stmt->fetchAll();
        
        return $result;
    }

    private static function placeholders($text, $count = 0, $separator = ",") {

        $result = array();
        if ($count > 0) {
            for ($x = 0; $x < $count; $x++) {
                $result[] = $text;
            }
        }

        return implode($separator, $result);
    }

}
