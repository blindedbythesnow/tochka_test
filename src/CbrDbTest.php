<?php
include_once 'CbrDb.php';
include_once 'DatabaseFactory.php';
include_once 'Config.php';
/*
function my_autoload($class) {
    include_once __DIR__ . "/" . $class . ".php";
}

spl_autoload_register("my_autoload");
*/
use \PHPUnit\Framework\TestCase;
/**
 * Description 
 * Создаём базу данных и таблицу
 * @author admind
 */

class CbrTest extends TestCase {
    //put your code here
    private $db;
    
     protected function setUp()
    {
        $this->db = new CbrDb();
    }
 
    protected function tearDown()
    {
        $this->db = NULL;
    }
    
    public function testsCreateDB() {
         $result = $this->db->createDB();
         $this->assertFalse($result);
    }
    
    
    public function testCreateDBTable() {
         $result = $this->db->createDBTable();
         $this->assertTrue($result);
    }
    
    
}
