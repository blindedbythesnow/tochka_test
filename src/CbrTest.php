<?php

include_once 'Cbr.php';
include_once 'CbrFactory.php';

use \PHPUnit\Framework\TestCase;
/**
 * Description of CurrentDateCbrrTest
 *
 *  PHP Fatal error:  Class 'SoapClient' not found 
 * 
 *  about error https://github.com/sebastianbergmann/phpunit/issues/970
 * 
 * @author admind
 */

class CbrTest extends TestCase {
    //put your code here
    private $currentDate;
    
     protected function setUp()
    {
        $this->currentDate = new Cbr();
    }
 
    protected function tearDown()
    {
        $this->currentDate = NULL;
    }
    
    public function testGetLastDate() {
         $result = $this->currentDate->getLastDate();
         $this->assertTrue($result);
    }
    
}
